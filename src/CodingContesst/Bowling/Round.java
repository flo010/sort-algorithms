package CodingContesst.Bowling;

public class Round {
    private int round;
    private int throw1;
    private int throw2;
    private int throw3;
    private boolean AllTen;

    public Round(int round, int throw1, int throw2, boolean Allten) {
        this.round = round;
        this.throw1 = throw1;
        this.throw2 = throw2;
        this.AllTen = Allten;
    }
    public Round(int round, int throw1, int throw2, int throw3, boolean Allten) {
        this.round = round;
        this.throw1 = throw1;
        this.throw2 = throw2;
        this.throw3 = throw3;
        this.AllTen = Allten;
    }
    public Round(int round, int throw1, boolean allTen) {
        this.round = round;
        this.throw1 = throw1;
        AllTen = allTen;
    }

    public int getThrow1() {
        return throw1;
    }

    public void setThrow1(int throw1) {
        this.throw1 = throw1;
    }

    public int getThrow2() {
        return throw2;
    }

    public void setThrow2(int throw2) {
        this.throw2 = throw2;
    }

    public boolean isAllTen() {
        return AllTen;
    }


    public void setAllTen(boolean allTen) {
        AllTen = allTen;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public int getThrow3() {
        return throw3;
    }

    public void setThrow3(int throw3) {
        this.throw3 = throw3;
    }
}
