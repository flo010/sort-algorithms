package List;

public class Stack{
    Node Top ;

    public void push(int value) {
        Node n= new Node();
        n.setValue(value);
        if(Top==null){
           Top = n;
        }
        else{
            n.setBefore(Top);
            Top =n;
        }
        }

    public int pop() {
        int value;
        value=Top.getValue();
        Top = Top.getBefore();
        return value;
    }


    public void clear() {
        Top = null;
    }


}
