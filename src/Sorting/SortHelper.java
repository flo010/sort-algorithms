package Sorting;


import java.util.Random;

public class SortHelper {

public  static  int[] generateArray(int Size,int Boundary)
{
    Random random = new Random();
    int[] arr = new int[Size];
    for(int i = 0; i < Size; i++) {
        arr[i]= random.nextInt(Boundary);
    }
    return arr;
}
}
