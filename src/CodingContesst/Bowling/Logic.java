package CodingContesst.Bowling;


import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;
public class Logic {
    private LinkedList<Round> Game = new LinkedList<>();
    private int  pointvaluenow = 0;
    private int roundsmax = 0;
    private Queue<Integer> points = new LinkedList<>();
    public int input(String Input) {
        String[] temp = Input.split(":");
        String[] temp1 = temp[1].split(",");
        for (int i = 0; i < temp1.length; i++) {
            points.add(Integer.parseInt(temp1[i]));
        }
        int rounds = Integer.parseInt(temp[0]);
        roundsmax = rounds;
        return rounds;
    }

    public void sort_rounds(int rounds){
        if(rounds == 0){
            return;
        }
        sort_rounds(rounds-1);
        if(roundsmax != rounds) {
            if (Integer.parseInt(points.peek().toString()) == 10) {
                Round r = new Round(rounds, Integer.parseInt(points.poll().toString()), true);
                Game.add(r);
            } else {
                int point1 = Integer.parseInt(points.poll().toString());
                int point2 = Integer.parseInt(points.poll().toString());
                if (point1 + point2 == 10) {
                    Round r = new Round(rounds, point1, point2, true);
                    Game.add(r);
                } else {
                    Round r = new Round(rounds, point1, point2, false);
                    Game.add(r);
                }
            }
        }
        else{
            if (Integer.parseInt(points.peek().toString()) == 10) {
                Round r = new Round(rounds, Integer.parseInt(points.poll().toString()), Integer.parseInt(points.poll().toString()), Integer.parseInt(points.poll().toString()), true);
                Game.add(r);
            } else{
                int point1 = Integer.parseInt(points.poll().toString());
                int point2 = Integer.parseInt(points.poll().toString());
                if (point1 + point2 == 10) {
                    Round r = new Round(rounds, point1, point2, Integer.parseInt(points.poll().toString()), true);
                    Game.add(r);
                } else {
                    Round r = new Round(rounds, point1, point2, false);
                    Game.add(r);
                }
            }
        }
        return;
    }

    public int value_round(int round){
        int r = 0;
        int points = 0;
        Round s = Game.get(r);
        while(s.getRound() != round){
            r = r+1;
            s = Game.get(r);
        }
        if(round == roundsmax){
          if(s.isAllTen() == true){
              if(s.getThrow1() == 10){
                  if(s.getThrow2() == 10)
                  {
                    points = 30;
                  }
                  else{
                      points = 10 +  (s.getThrow2() + s.getThrow3());
                  }
              }
              else{
                  points = 10 +  s.getThrow3();
              }
          }
          else{
               points = s.getThrow1() + s.getThrow2();
          }
        }
        else if( s.isAllTen() == true){
            Round next = Game.get(r+1);
                if(s.getThrow2() == 0) {
                    if (next.isAllTen() == true && next.getThrow2() == 0) {
                        points = 20;
                        if (Game.get(r + 2) != null) {
                            next = Game.get(r + 2);
                            points = points + next.getThrow1();
                        }
                    }
                    else {
                        points = 10 + next.getThrow1() + next.getThrow2();
                    }
                }
                else{
                    points = 10 + next.getThrow1();
                }
        }
        else{
            points = s.getThrow1() + s.getThrow2();
        }
        return points;
    }

    public String Output(int rounds){
        String Output = "";
        if(rounds>1) {
            Output = Output(rounds - 1);
        }
        pointvaluenow = pointvaluenow + value_round(rounds);
        System.out.println(pointvaluenow);
        Output = Output + pointvaluenow + ",";
        return Output;
    }
}
