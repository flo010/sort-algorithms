package Sorting;

public class Main {
    public static void main(String Args[]){
        int arr[] =SortHelper.generateArray(10,100);
        System.out.println("Unsorted Array");
        printArray(arr);
        Sorter sorter = new bucketsort();
        int sorted[] = sorter.sort(arr);
        System.out.println("Sorted Array");
        printArray(sorted);
    }

    private static void printArray(int[] arr){
        for(int value: arr)
        {
            System.out.print(value + " - ");

        }
        System.out.println();
    }
}
