package List;

public class Node{
    private int value;

    public Node getBefore() {
        return before;
    }

    public void setBefore(Node before) {
        this.before = before;
    }

    public Node getAfter() {
        return after;
    }

    public void setAfter(Node after) {
        this.after = after;
    }

    private Node before;
    private Node after;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }



}
