package Sorting;

public class InsertionSort implements Sorter {
    @Override
    public int[] sort(int[] arr) {
        int temp;
        int i = 0;
        int pointback;
        boolean trade = false;
        for (int j = 1; j < arr.length-1; j++) {
            if(arr[j]>arr[j+1]){
                temp =arr[j];
                arr[j]=arr[j+1];
                arr[j+1] = temp;
                trade = true;
            }
            if(trade = true &&  j !=0){
                pointback = 1;
                do{

                    if(j!=0) {
                         i = j - pointback;
                    }
                    if(i>=0) {
                        if (arr[i] > arr[i + 1]) {
                            temp = arr[i];
                            arr[i] = arr[i + 1];
                            arr[i + 1] = temp;
                            trade = true;
                            pointback = pointback + 1;
                        } else {
                            trade = false;
                        }
                    }
                    else{trade =false;}
                }while(trade==true);
            }

        }
        return arr;
    }
}

