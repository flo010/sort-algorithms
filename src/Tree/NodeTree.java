package Tree;

public class NodeTree {
    private NodeTree child1;
    private NodeTree child2;
    private int value;

    public NodeTree getChild1() {
        return child1;
    }

    public void setChild1(NodeTree child1) {
        this.child1 = child1;
    }

    public NodeTree getChild2() {
        return child2;
    }

    public void setChild2(NodeTree child2) {
        this.child2 = child2;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

}
