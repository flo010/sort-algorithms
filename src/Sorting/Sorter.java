package Sorting;

public interface Sorter {
    int[] sort(int[] arr);
}

