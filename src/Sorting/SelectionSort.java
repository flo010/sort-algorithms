package Sorting;

public class SelectionSort implements Sorter{

    @Override
    public int[] sort(int[] arr) {
        int lowest_value;
        int lowest_value_index;

        for(int i=0;i<arr.length;i++) {
            lowest_value=arr[i];
            lowest_value_index = i;
            for(int j=i+1;j<arr.length;j++) {
               if(lowest_value>arr[j]) {
                   lowest_value = arr[j];
                   lowest_value_index = j;
               }
            }
            if(lowest_value_index != i)
            {
                arr[lowest_value_index] = arr[i];
                arr[i] = lowest_value;
            }
        }
        return arr;
    }
}
