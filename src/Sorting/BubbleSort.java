package Sorting;

public class BubbleSort implements Sorter {
    @Override
    public int[] sort(int[] arr) {
        int temp;
        for(int j=1; j<arr.length; j++) {
            for (int i = 0; i < arr.length-j; i++) {
                if(arr[i]>arr[i+1]){
                    temp =arr[i];
                    arr[i]=arr[i+1];
                    arr[i+1] = temp;
                }
            }

        }
        return arr;
    }

}
