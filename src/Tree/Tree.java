package Tree;


import List.Node;

import java.util.HashMap;
import java.util.LinkedList;

public class Tree {
    NodeTree root;
    HashMap<Integer, LinkedList<NodeTree>> Out = new HashMap();
    public void add(int value){
        boolean loop = true;
        NodeTree n= new NodeTree();
        n.setValue(value);
        if(root== null){
            root = n;
        }
        else{
            NodeTree x = root;
            while(loop) {
                if (x.getValue() < n.getValue()) {
                    if (x.getChild1() == null) {
                        x.setChild1(n);
                        return;
                    } else {
                        x = x.getChild1();
                    }

                } else {
                    if (x.getChild2() == null) {
                        x.setChild2(n);
                        return;
                    } else {
                        x = x.getChild2();
                    }
                }
            }
        }
    }

    public NodeTree search(int value, NodeTree n){
        if(n.getValue()== value){
            return n;
        }
        if(n.getValue() < value){
            search(value, n.getChild1());
        } else if (n.getValue() > value){
            search(value, n.getChild2());
        }
        else{
            return null;
        }
       return null;
    }

    public void print(){
        int Layer= Out.size();
        print_before(Layer);

    }
    public void printLayer(int Layer){

    }
    public void print_before(int Layer){
        for(int i=0; i<=Layer; i++)
        {
            System.out.print(" ");
        }
    }
    private void enLayer(int Layer,NodeTree n ){
        Out.putIfAbsent(Layer,new LinkedList<NodeTree>());
        Out.get(Layer).add(n);
        if(n.getChild1() == null && n.getChild2() == null){
            return;
        }
        if(n.getChild1() != null){
            enLayer(Layer+1,n.getChild1());
        }
        if(n.getChild2() != null){
            enLayer(Layer+1,n.getChild1());
        }
    }

    public void clear(){
        root = null;
    }
}
