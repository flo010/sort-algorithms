package List;

public class Queue {
    Node QueueLast = new Node();

    public void enqueue(int value){
        Node n = new Node();
        n.setValue(value);
        if(QueueLast == null){
            QueueLast = n;
        }
        else if(QueueLast.getBefore() == null){
            QueueLast.setBefore(n);
        }
        else{
            Node x = QueueLast;
            while (x.getBefore()!=null)
            {
                x = x.getBefore();
            }
            x.setBefore(n);
        }
    }
    public int dequeue(){
        int x = QueueLast.getValue();
        QueueLast = QueueLast.getBefore();
        return x;
    }
}
