package List;

public interface List {
    public int get(int index);
    public void add(int value);
    public void getAll();
    public void remove(int index);
    public void clear();


}
